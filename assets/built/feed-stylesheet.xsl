<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title><xsl:value-of select="/rss/channel/title"/> RSS Feed</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <style type="text/css">
                    body {
                        font-family: Inter,-apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif;
                        font-size: 1.3rem;
                        color: #333;
                        line-height: 1.5;
                        text-rendering: optimizespeed;
                        padding: 0;
                        margin: 0;
                    }
                    a, a:link, a:visited {
                        color: #005C82;
                        text-decoration: none;
                    }
                    a:hover {
                        color: #000;
                    }
                    h1, h2, h3, p {
                        margin-top: 0;
                        margin-bottom: 20px;
                    }
                    h3 {
                        font-style: italic;
                    }
                    #content {
                        max-width: 720px;
                        margin: 0 auto;
                        background: #FFF;
                        padding: 30px;
                        box-sizing: border-box;
                    }
                    #channel-image {
                        float: right;
                        width: 100%;
                        max-width: 200px;
                        margin-bottom: 20px;
                        margin-left: 20px;
                    }
                    #channel-image img {
                        width: 100%;
                        height: auto;
                        border-radius: 100%;
                    }
                    #channel-header {
                        margin-bottom: 20px;
                    }
                    .channel-item {
                        clear: both;
                        border-top: 1px solid #E5E5E5;
                        padding: 20px;
                    }
                    .episode-image img, .channel-item img, audio {
                        margin-bottom: 1rem;
                        margin-left: auto;
                        margin-right: auto;
                        display: block;
                        width: 100%; 
                        max-width: 500px;
                        border-radius: 10px;
                    }
                    .episode_meta {
                        font-size: 1rem;
                        text-align: center;
                    }

                    @media (min-width: 768px) {
                        #content {
                            padding: 50px;
                        }
                    }
                </style>
            </head>
            <body>
                <div id="content">
                    <div id="channel-header">
                        <h1>
                            <xsl:if test="/rss/channel/image">
                                <div id="channel-image">
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="/rss/channel/image/link"/>
                                        </xsl:attribute>
                                        <img>
                                            <xsl:attribute name="src">
                                                <xsl:value-of select="/rss/channel/image/url"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="title">
                                                <xsl:value-of select="/rss/channel/image/title"/>
                                            </xsl:attribute>
                                        </img>
                                    </a>
                                </div>
                            </xsl:if>
                            <xsl:value-of select="/rss/channel/title"/>
                        </h1>
                        <p>
                            <xsl:value-of select="/rss/channel/description"/>
                        </p>
                        <p>
                            <a>
                                <xsl:attribute name="href">
                                    <xsl:value-of select="/rss/channel/link"/>
                                </xsl:attribute>
                                <xsl:attribute name="target">_blank</xsl:attribute>
                                Visitez le site du podcast &#x0226B;
                            </a>
                        </p>
                    </div>
                    <xsl:for-each select="/rss/channel/item">
                        <div class="channel-item">
                            <h2>
                                <a>
                                    <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                                    <xsl:attribute name="target">_blank</xsl:attribute>
                                    <xsl:value-of select="title"/>
                                </a>
                            </h2>
							<xsl:if test="image">
                            <a>
                                <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                            <img>
								<xsl:attribute name="src">
									<xsl:value-of select="image/url"/>
								</xsl:attribute>
								<xsl:attribute name="title">
									<xsl:value-of select="image/title"/>
								</xsl:attribute>
							</img>
                            </a>
							
							</xsl:if>
                            <xsl:if test="description">
                                <p>
                                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                                </p>
                            </xsl:if>
                            <audio>
                                <xsl:attribute name="controls"></xsl:attribute>
                                <xsl:attribute name="src">
                                    <xsl:value-of select="enclosure/@url" />
                                </xsl:attribute>
                            </audio>
                            <p class="episode_meta">
                                    <a>
                                        <xsl:attribute name="href"><xsl:value-of select="enclosure/@url"/>?ref=download</xsl:attribute>
                                        <xsl:attribute name="download"/>
                                        Télécharger l'épisode
                                    </a> |
                                    <a>
                                        <xsl:attribute name="href"><xsl:value-of select="enclosure/@url"/>?ref=new_window</xsl:attribute>
                                        <xsl:attribute name="target">_blank</xsl:attribute>
                                        Lire dans une nouvelle fenêtre
                                    </a> |
                                    Taille du fichier : <xsl:value-of select='format-number(number(enclosure/@length div "1024000"),"0.0")'/>Mo
                            </p>
                        </div>
                    </xsl:for-each>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
