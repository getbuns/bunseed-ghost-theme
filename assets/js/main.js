const html = $('html');
const hostname = window.location.hostname;
let member = {};

$(function() {

    darkMode();

    carousel();

    resizeChat();

    $(window).on('resize', function() {
        if ($(".owl").not(".owl-loaded").length) {
            carousel();
        }
        resizeChat();
    });

    dataVideo();

    timeAgo(); // a laisser après de dataVideo

    postActions();

    // GET MEMBER INFO
    fetch("/api/member/")
    .then(response => response.json())
    .then(data => {
        member = data;
        postActions_addData();
    })
    .catch(function(){
        member = {"type":"error"};
        postActions_addData();
    });

    friendsPosts();

    getPlaybackPosition();

    myListActions();

    loadPlaylist();

    fullscreen();

    keyboardNav();

    modal_Construct();
    $(window).on('hashchange', function() {
        modal();
    });
    $(window).on('popstate', function() {
        modal();
      });
    modal();

    removeUnaccessDom();

});

function darkMode() {
    $('.toggle-track').on('click', function() {
        if (html.hasClass('dark-mode')) {
            html.removeClass('dark-mode');
            localStorage.setItem('alto_dark', false);
        } else {
            html.addClass('dark-mode');
            localStorage.setItem('alto_dark', true);
        }
    });
}
function slugify(str) {
    if (str !== undefined){
        str = str.replace(/^\s+|\s+$/g, ''); // enlever les espaces en début et fin de chaîne
        str = str.toLowerCase(); // convertir tous les caractères en minuscules
        str = str.replace(/[^\w\s-]/g, ''); // enlever tous les caractères qui ne sont pas des lettres, des chiffres, des espaces ou des tirets
        str = str.replace(/[\s_-]+/g, '-'); // remplacer les espaces et les tirets par des tirets
        return str;
    }
    return '';
}
  
function constructShowmoreArticles(carousel,articlesByPage = 4){

    var totalArticles = carousel.find('article').length;

    if (totalArticles > articlesByPage){

        var carouselID = slugify(carousel.parent().find("h5").text()) + carousel.find('article').first().data('id');
        var numPage = sessionStorage['numPageCarousel'+carouselID] ? Number(sessionStorage['numPageCarousel'+carouselID]) : 0;
        
        var debut = numPage * articlesByPage;
        var fin = debut + articlesByPage;

        carousel.find('article').slice(debut, fin).show();
        carousel.find('article').slice(fin, totalArticles).hide();

        if (!carousel.parent().find('button.showmore').length){
            if (fin >= totalArticles) {
                carousel.after($(`<button tabindex="0">Replier <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-up-fill" viewBox="0 0 16 16">
                <path d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
              </svg></button>`).addClass('showmore').click(function(){buttonShowmoreArticles(this,articlesByPage)}));
            } else {
                carousel.after($(`<button tabindex="0">Voir plus <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
              </svg></button>`).addClass('showmore').addClass('more').click(function(){buttonShowmoreArticles(this,articlesByPage)}));
            }
        }
    }
}
function buttonShowmoreArticles(button,articlesByPage = 4){
    if ($(button).hasClass('more')){
        ShowmoreArticles($(button).prev('.carousel'),articlesByPage);
    }
    else {
        ShowlessArticles($(button).prev('.carousel'),articlesByPage);
    }
}
function ShowmoreArticles(carousel,articlesByPage = 4){
    var carouselID = slugify(carousel.parent().find("h5").text()) + carousel.find('article').first().data('id');

    sessionStorage['numPageCarousel'+carouselID] = Number(sessionStorage['numPageCarousel'+carouselID] ? sessionStorage['numPageCarousel'+carouselID] : 0) + 1;
    var numPage = Number(sessionStorage['numPageCarousel'+carouselID]);

    var totalArticles = carousel.find('article').length;
    var debut = numPage * articlesByPage;
    var fin = debut + articlesByPage;

    var buttonShowMore = carousel.parent().find('button.showmore');

    if (numPage == 0){
        carousel.find('article').slice(fin, totalArticles).hide();
    } else {
        carousel.find('article').slice(debut, fin).fadeIn(200);
    }

    if (fin >= totalArticles) {
        buttonShowMore.removeClass("more").html(`Replier <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-up-fill" viewBox="0 0 16 16">
        <path d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
      </svg>`);
    }
}
function ShowlessArticles(carousel,articlesByPage = 4){
    var carouselID = slugify(carousel.parent().find("h5").text()) + carousel.find('article').first().data('id');
    sessionStorage['numPageCarousel'+carouselID] = 0;
    var numPage = 0;
    
    var totalArticles = carousel.find('article').length;
    var debut = numPage * articlesByPage;
    var fin = debut + articlesByPage;

    var buttonShowMore = carousel.parent().find('button.showmore');

    carousel.find('article').slice(fin, totalArticles).hide();
    $([document.documentElement, document.body]).animate({
        scrollTop: carousel.find('article').eq(articlesByPage - 2).offset().top
    }, 0);

    buttonShowMore.addClass("more").html(`Voir plus <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
    <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
  </svg>`);
}

function destroyShowmoreArticles(carousel){
    carousel.css('visibility', 'visible');
    carousel.find('article').show();
    carousel.parent().find('button.showmore').remove();
}




function carousel() {

    var imageHeight, nav;

    function moveNav(target) {
        imageHeight = target.find('.post-image').height();
        nav = target.find('.owl-prev, .owl-next');
        nav.css({
            top: imageHeight / 2 + 'px',
            opacity: 1,
        });
    }

    $('.carousel').each(function(){

        const carousel = $(this);
        settings = carousel.data('carousel') ? carousel.data('carousel') : undefined;

        if (settings == undefined){
            if (carousel.hasClass('playlist')){
                settings = {
                    minWidth : null,
                    responsive : {
                        0: {
                            items: 2,
                            slideBy: 2,
                            nav: true,
                        },
                        768: {
                            items: 3,
                            slideBy: 3,
                        },
                        992: {
                            items: 4,
                            slideBy: 4,
                        },
                    }
                };
            }
            else if (carousel.hasClass('authors')){
                settings = {
                    minWidth : null,
                    responsive : {
                        0: {
                            items: 3,
                            slideBy: 3,
                            nav: true,
                            margin: 0,
                        },
                        768: {
                            items: 4,
                            slideBy: 4,
                            margin: 12,
                        },
                        992: {
                            items: 5,
                            slideBy: 5,
                        }
                    }
                };
            }
            else {
                settings = {
                    minWidth : 768,
                    responsive : {
                        0: {
                            items: 1,
                            slideBy: 1,
                            nav: true,
                        },
                        768: {
                            items: 3,
                            slideBy: 3,
                        },
                        992: {
                            items: 4,
                            slideBy: 4,
                        }
                    }
                };
            }
        }

        if (settings.minWidth !== null){

            if (window.matchMedia("(min-width: "+settings.minWidth+"px)").matches){
                makeCarousel(carousel,settings);
            } else {
                constructShowmoreArticles(carousel,getArticlesByPage(settings.responsive));
            }
            
        } else {
            makeCarousel(carousel,settings);
        }

    });

    function makeCarousel(carousel,settings){

        carousel.owlCarousel({
            dots: false,
            margin: 12,
            nav: true,
            smartSpeed: 60,
            lazyLoad: true,
            navText: [
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M20.547 22.107l-6.107-6.107 6.107-6.12-1.88-1.88-8 8 8 8 1.88-1.893z"></path></svg>',
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M11.453 22.107l6.107-6.107-6.107-6.12 1.88-1.88 8 8-8 8-1.88-1.893z"></path></svg>',
            ],
            onInitialized: function() {
                carousel.css('visibility', 'visible'); // c'était là pour les authors ?
                moveNav(carousel);
                if (settings.minWidth != null){
                    destroyShowmoreArticles(carousel,getArticlesByPage(settings.responsive));
                }
            },
            onResized: function() {
                moveNav(carousel);
                if (settings.minWidth !== null){ if (window.matchMedia("(max-width: "+settings.minWidth+"px)").matches) {
                    carousel.trigger('destroy.owl.carousel');
                    constructShowmoreArticles(carousel,getArticlesByPage(settings.responsive));
                }}
            },
            onRefreshed: function() {
                moveNav(carousel);
            },
            responsive: settings.responsive,
        });
    }

    function getArticlesByPage(responsive){
        let keys = Object.keys(settings.responsive);
        let maxKey = Math.max(...keys);
        let maxItems = settings.responsive[maxKey].items;
        return maxItems;
    }

}

function simplifyDuration(duration) {
    let parts = duration.split(':');
    if (parts[0] == 0) { parts.shift(); }
    if (parts.length == 3) {
        return Math.round(parts[0]) + ":" + parts[1] + ":" + parts[2];
    } else {
        return Math.round(parts[0]) + ":" + parts[1];
    }
}

function getDuration(duration) {
    d = Number(duration.replace('PT', '').replace('S', ''));
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + ":" : "";
    var mDisplay = m > 0 ? m + ":" : "";
    var sDisplay = s > 0 ? s : "";

    return hDisplay + mDisplay + sDisplay;
}

function parseSellValues(value) {
    // Initialiser les valeurs par défaut
    let tag = value;
    let type = '';
    let period = false;
    let currency = '';
    let amount = 0;
    let pwyw = false;
    let pwyw_amount = 0;
    let serie = false;

    let label = '';
    let title = '';
    let button = '';

    let remainingValue = value.replace('tag-hash-','');

    // Split by '-' to extract components
    let components = value.split('-');

    // Extraire le type
    type = components.filter(item => ['buy', 'rent', 'crowdfunding'].includes(item))[0];
    if (type) {
        let typeIndex = components.indexOf(type);
        let nextComponent = components[typeIndex + 1];

        // Si le prochain élément après le type n'est pas un montant, retourner false
        if (!nextComponent || !/^\d+([a-zA-Z]+)$/.test(nextComponent)) {
            return false;
        }
        remainingValue = remainingValue.replace(type, '');
    }

    // Extraire le montant et la monnaie
    let amountComponent = components.find(item => /(\d+([a-zA-Z]+))$/.test(item));
    if (amountComponent) {
        currency = currencyToSymbol( amountComponent.match(/[a-zA-Z]+$/)[0] );
        amount = parseFloat(amountComponent.replace(currency, ''));
        remainingValue = remainingValue.replace(amountComponent, '');
    }

    // Extraire period pour les locations
    let periodRegex = /(\d+h|\d+d|\d+w)/;
    let periodComponent = components.find(item => periodRegex.test(item));
    if (periodComponent) {
        period = periodComponent;
        remainingValue = remainingValue.replace(periodComponent, '');
    }

    // Vérifier si pwyw est présent et extraire le montant correspondant
    let pwywIndex = components.indexOf('pwyw');
    if (pwywIndex !== -1) {
        pwyw = true;
        if (components.length > pwywIndex + 1 && /^\d+/.test(components[pwywIndex + 1])) {
            pwyw_amount = parseFloat(components[pwywIndex + 1].replace(/[a-zA-Z]+$/, ''));
            remainingValue = remainingValue.replace(components[pwywIndex + 1], ''); // Remove the amount from remaining value
        }
        remainingValue = remainingValue.replace('pwyw', '');
    }


    // Après avoir retiré tous les éléments reconnus, ce qui reste est le slug de la série
    remainingValue = remainingValue.replace(/-+/g, '-'); // Remplacer les multiples '-' par un seul
    if (remainingValue.startsWith('-')) {
        remainingValue = remainingValue.slice(1);
    }
    if (remainingValue.endsWith('-')) {
        remainingValue = remainingValue.slice(0, -1);
    }

    if (remainingValue) {
        serie = remainingValue;
    }


    // LABEL / TITLE
    if (type == "buy"){
        label   = pwyw ? " Prix libre" 
                :  " Acheter "+amount+currency;

        title   = pwyw ? "Acheter à prix libre"+ ( pwyw_amount != 0 ? " (à partir de " + pwyw_amount + currency + ")" : "" )
                : "Acheter "+amount+currency;

        button  = pwyw ? "Acheter à prix libre"
                : "Acheter "+amount+currency;
        
    }
    else if (type == "rent"){
        label   = pwyw ? " Prix libre"
                : " Louer "+amount+currency;

        title   = pwyw ? "Louer à prix libre"+ ( pwyw_amount != 0 ? " (à partir de " + pwyw_amount + currency + ")" : "" ) 
                : "Louer "+amount+currency+" pendant "+period.replace("d"," jours").replace("w"," semaines");

        button   = pwyw ? "Louer à prix libre" 
                : "Louer "+amount+currency+" ("+period.replace("d"," jours").replace("w"," semaines")+")";
    }


    return {
        tag,
        type,
        period,
        currency,
        amount,
        pwyw,
        pwyw_amount,
        serie,
        label,
        title,
        button
    };
}

function currencyToSymbol(cur) {
    const currencyMap = {
        'eu': '€',
        'usd': '$',
        'gbp': '£',
        'cad': '$',
        'aud': '$',
        'jpy': '¥',
    };

    return currencyMap[cur] || cur; // Si la devise n'est pas trouvée, retournez la devise elle-même
}


/**
 * Convert à string formated as key<SEPARATOR>value\nkey<SEPARATOR>value... to an object
 *
 */
function convertFormatedStringToObject(source, separator = ',') {

    const durationRegexp = new RegExp("^\\d{2}:\\d{2}:\\d{2}$");

    // If source is just a duration, return it as the 'duration' property
    if (durationRegexp.test(source)) {
        return { duration: source };
    }

    const regexpToExtractData = new RegExp(`\\s*(?<key>[a-z]+)\\s*${separator}\\s*(?<value>.+)`, "gm");
    let objectFromString = {};
    const r = Array.from(source.matchAll(regexpToExtractData));
    r.map(e => {
        objectFromString[e.groups.key] = e.groups.value.trim();
    });
    return objectFromString;
}

function spreadDateInTemplate($articleJqueryEl, date) {
    if (!date) { return; }
    $articleJqueryEl.find("time").attr('datetime', date);

    // POST DATE REPRECISEE
    if (!$articleJqueryEl.hasClass('.carousel')) {
        const dateMoment = moment(date, "YYYY-MM-DD hh:mm").format("Do MMM YYYY");
        $articleJqueryEl.find("time").html(dateMoment);
    }
}

function spreadSellsInTemplate($articleJqueryEl, sells) {
    if (!sells.length) { return; }
    
    sells.sort((a, b) => {
        // Trier par pwyw en true en premier
        if (a.pwyw && !b.pwyw) return -1;
        if (!a.pwyw && b.pwyw) return 1;
    
        // Trier par type : rent avant buy
        if (a.type === 'rent' && b.type !== 'rent') return -1;
        if (a.type !== 'rent' && b.type === 'rent') return 1;
    
        if (a.type === 'buy' && b.type !== 'buy') return -1;
        if (a.type !== 'buy' && b.type === 'buy') return 1;
    
        // Trier par serie en true après buy
        if (a.serie && !b.serie) return -1;
        if (!a.serie && b.serie) return 1;
    
        // Trier par amount croissant
        return a.amount - b.amount;
    });

    // Mettre dans l'étiquette media-access si elle existe
    $accessEl = $articleJqueryEl.find(".post-media-access");
    if ($accessEl !== undefined && !$accessEl.text().includes("✅")){

        let label_tile = '';

        if (sells.length > 1){
            label_tile = "- "+sells.map(item => item.title).join("\n- ");
        } else {
            label_tile = sells[0].title;
        }

        $accessEl.html(sells[0].label);
        $accessEl.attr('title', label_tile + ($accessEl.attr('title') !== undefined ? ("\n" + $accessEl.attr('title')) : ""));
    }

    $buttonEl = $articleJqueryEl.find(".gh-post-upgrade-cta .gh-btn");
    if ($buttonEl !== undefined){

        $buttonEl.wrap('<div class="gh-multi-btn"></div>');
        let textColor = $buttonEl.css('color');

        $h2El = $articleJqueryEl.find(".gh-post-upgrade-cta h2");
        $h2El.text("Disponible à la vente et "+$h2El.text().substr($h2El.text().indexOf(" ") + 1));

        sells.forEach(e => {
            let buttonHtml = `<a class="gh-btn gh-sell" style="color:`+textColor+`" title="`+e.title+`" href="#post=`+$articleJqueryEl.data("id")+`&tag=`+e.tag+(member.id ? `&memberid=`+member.id+`&memberemail=`+member.email:``)+`" onclick="javascript:alert('Vente prochainement disponible.')">`+e.button+`</a>`;
            $buttonEl.before(buttonHtml);
        });
    }

}

/**
 * Spread the duration ONLY for article in carousel
 * @todo do something if article is not in carousel ?
 *
 * @param {JqueryElement} $articleJqueryEl a jquery element of an article
 * @param {string} duration the duration in HH:MM:SS or MM:SS or SS format
 *
 * @returns void
 *
 */
function spreadDurationInTemplate($articleJqueryEl, duration) {
    if (!duration && ( !$articleJqueryEl.parents('.carousel').length
                    || !$articleJqueryEl.parents('.post-feed').length)) { return; }
                    
    const durationHtmlElementToAdd = `
        <span class="post-media-duration"> ${simplifyDuration(duration)}  </span>
    `;
    $articleJqueryEl.find(".post-image-link").prepend(durationHtmlElementToAdd);
}

function dataVideo() {
    $('article').each(function() {
        doDataVideo($(this));
    });
}
function doDataVideo(e){

    // ANCIENNE VERSION
    /*  
    if (!e.data('video')) {
        return;
    }

    const rawArticleData = e.data('video');
    // RECUPERER LES DATAS
    let extractedArticleData = convertFormatedStringToObject(rawArticleData);
    
    // DATE
    spreadDateInTemplate(e, extractedArticleData.date);

    // DURATION
    spreadDurationInTemplate(e, extractedArticleData.duration);
    */

    let duration = e.attr('data-duration') !== undefined ? e.attr('data-duration') : undefined;
    let date;
    let sells = [];
    const classes = e.attr('class').split(' ');

    for (let cls of classes) {
        if (cls.startsWith("tag-hash-date-")) {
            const parts = cls.split('-');

            if (parts.length === 8) { // s'assurer que la longueur est correcte
                const year = parts[3];
                const month = parts[4];
                const day = parts[5];
                const hour = parts[6];
                const minute = parts[7];

                //date = new Date(year, month, day, hour, minute);
                date = year+'-'+month+'-'+day+' '+hour+':'+minute;
            }
        }

        if (cls.startsWith("tag-hash-buy-") || cls.startsWith("tag-hash-rent-") || cls.startsWith("tag-hash-crowdfunding-")) {
            sells.push(parseSellValues(cls));
        }
    }

    // DURATION
    spreadDurationInTemplate(e, duration);

    // DATE
    spreadDateInTemplate(e, date);

    // SELLS
    spreadSellsInTemplate(e, sells);

}

function timeAgo() {
    $("time").each(function() {
        doTimeAgo($(this));
    });
}
function doTimeAgo(e){

    const date = e.attr('datetime');
    const now = moment();
    const daysDiff = now.diff(date, 's');

    if (e.parents('.carousel').length) {
        e.html(moment(date, "YYYY-MM-DD hh:mm").fromNow());
    }
    
    if (daysDiff <= - 7 * 24 * 60 * 60) { // dans plus de 7jours
        e.find('.new').remove();
        e.prepend("<span class='new'>Prochainement</span>");
    }
    else if (daysDiff <= - 2 * 60 * 60) { // de 7jours à 2h
        e.find('.new').remove();
        e.prepend("<span class='new'>Bientôt</span>");
    }
    else if (daysDiff <= - 5 * 60) { // de 2h à 5min
        e.find('.new').remove();
        e.prepend("<span class='new soon'>Très bientôt</span>");
    }
    else if (daysDiff <= 0) { // de 5min à 0
        e.find('.new').remove();
        e.prepend("<span class='new about-to-start'>Ça va commencer</span>");
    }
    else if (daysDiff < 7 * 24 * 60 * 60) {
        e.find('.new').remove();
        e.prepend("<span class='new'>Nouveau</span>");
    }

    // Refresh if recent or soon
    if (Math.abs(daysDiff) < 20 * 60) { // en dessous de 20min, refresh/2s
        setTimeout(function() { doTimeAgo(e) }, 4 * 1000);
    }
    else if (Math.abs(daysDiff) < 2 * 60 * 60) { // en dessous de 2h, refresh/min
        setTimeout(function() { doTimeAgo(e) }, 60 * 1000);
    }

}

// ACTIONS
function postActions() {

    // DON
    $('#actions .give a').click(function() {
        var left = (screen.width - 550) / 2;
        var top = (screen.height - 650) / 2;
        window.open($(this).attr('href'), "_blank", "toolbar=no,scrollbars=yes,resizable=yes,top=" + top + ",left=" + left + ",width=550,height=600");
        return false;
    });

    // SHARE
    if (navigator.share /*&& navigator.userAgent.toLowerCase().match(/mobile|tablet/i)*/) {
        $('#actions .share a').click(function() {
            navigator.share({
                title: $(this).data('title'),
                text: $(this).data('text'),
                url: $(this).data('url'),
            })
                .then(() => console.log('Successful share'))
                .catch((error) => console.log('Error sharing', error));
            return false;
        });
    } else {
        $('#actions .share a').click(function() {
            var left = (screen.width - 550) / 2;
            var top = (screen.height - 650) / 2;
            window.open($(this).attr('href'), "_blank", "toolbar=no,scrollbars=yes,resizable=yes,top=" + top + ",left=" + left + ",width=550,height=600");
            return false;
        });
    }

    // COMMENT SCROLL
    $('#actions .comment a[href="#comments"]').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: parseInt($("#comments").offset().top,10) + parseInt($('#comments iframe').contents().find('form').offset().top,10) - parseInt($('body').css('padding-top'),10)
        }, 0);
        $('#comments iframe').contents().find('form').trigger("click");
    });

}
function postActions_addData(){

    // DON WITH STRIPE
    $('#actions .give a[href*="stripe.com"]').each(function(){
        if (member.type !== "error"){
            const href = $(this).attr('href');
            $(this).attr('href',href + "?prefilled_email=" + member.email);
        }
    });

    // COMMENT WITH TALLY
    $('a[href*="tally-open"]').each(function(){

        const data = (member.type !== "error" ? email="email="+member.email+"&name="+member.name+"&":"")+"origin="+location.href.split(location.host)[1];
        
        var src = $(this).attr('href');

        if (src.includes("#")){
            var href = src.split("#");

            if (src.includes("tally.so") && !src.includes("tally.so/r/")){
                var urlParams = new URLSearchParams(href[1]);
                var tallyID = urlParams.get('tally-open');
                href[0] = "https://tally.so/r/"+tallyID;
            }
            
            $(this).attr(
                'href',
                (href[0].includes("tally.so/r/")
                    ? href[0] + "?" + data + "#" + href[1] + "&" + data
                    : href[0] + "#" + href[1] + "&" + data
                )
            );
        }
        else {
            $(this).attr(
                'href',
                src + "?" + data
            );
        }

    });
    
    // TALLY IFRAME
    $('iframe[src*="tally.so"]').each(function(){

        const data = (member.type !== "error" ? email="email="+member.email+"&name="+member.name+"&":"")+"origin="+location.href.split(location.host)[1];
        
        var src = $(this).attr('src');

        $(this).attr(
            'src',
            src + "&" + data
        );

    });

}

function playlistTitles() {
    $('.title-tag').each(function() {
        const tag = $(this).data('tag');
        $(this).html($('.post-tag-' + tag).first().text());
    })
}

/**
 * try to extract RSS feed from url in parameter. Currently, just add `feed` at the end
 * of url if url doesn't include `rss|atom|feed`
 *
 * @param {string} websiteUrl the url to find RSS feed
 * @return {string} the url which include RSS feed path
 */
function extractRssFeedUrlFromWebsite(websiteUrl) {

    const regexContainsFeed = /rss|atom|feed/i;
    if (!regexContainsFeed.test(websiteUrl)) {
        if (!websiteUrl.endsWith('/')) { websiteUrl = websiteUrl + "/"; }
        if (!websiteUrl.endsWith('feed/')) { websiteUrl = websiteUrl + "feed/"; }
        return websiteUrl;
    }
    return websiteUrl;
}

function extractRssFeedUrlFromMultipleWebsite(websiteUrlArray) {
    return websiteUrlArray.map(websiteUrl => extractRssFeedUrlFromWebsite(websiteUrl));
}

/**
 * Fetch RSS Feeds from urls in the array in parameter. Currently, wait for all http call
 * before return
 *
 * @param {Array<string>} url Array of RSS feeds Urls 
 * @return {Array<RssParserItem} All the feeds return by RSSParser (@see rss-parser)
 */
async function getRssFeedFromUrl(urls) {
    let parser = new RSSParser({
        // timeout: 1000,
        customFields: {
            item: ['image', 'data_video', ['media:status', 'status'], ['media:videaste', 'videaste']]
        }
    });
    let feedsPromise = [];
    urls.forEach(url => {
        feedsPromise.push(
            new Promise((resolve) => {
                parser.parseURL(url)
                    .then(feed => resolve(feed))
                    .catch(error => {
                        console.error(`Erreur lors de l'analyse de l'URL ${url}:`, error);
                        resolve(null); // Retourne null en cas d'erreur, pour ne pas bloquer les autres promesses
                    });
            })
        );
    });
    const results = await Promise.all(feedsPromise);
    return results.filter(result => result !== null); // Supprime les valeurs null du tableau de résultats
}

/**
 * format items by adding feed object (avatar, title and link) into the item
 * and extract image from image.url[0]. At the end, sort items according to 
 * sortType
 * @param {Array<RssParserItem} rssFeedsItemsInArray all rss feeds items in Array
 * @param {string} sortType sort type (desc, asc, random currently supported)
 *
 * @return {Array} All the feeds items formatted and sorted
 */
function formatAndSortItemsFromRssFeed(rssFeedsItemsInArray, nbItemsByFeed = 12) {
    let allItemsFeedArray = rssFeedsItemsInArray.flatMap(i => {
        i.items.splice(nbItemsByFeed, i.items?.length);
        i.items.forEach(it => {
            if (it?.videaste?.$?.duration !== undefined){
                it.data_video = it?.videaste?.$?.duration;
            }
            it.image = it.image?.url[0];
            it.status = it?.status?.$?.state;
            it.feed = {
                avatar: i.image.url,
                title: i.title,
                link: i.link
            };
        });
        return i.items;
    });
    return allItemsFeedArray;
}

// LOAD RSS !!
function friendsPosts() {
    if (!$('.friends').length) { return; }

    $('.friends').each(async function() {

        const friendscarousel = $(this).find('.carousel');

        // GET FEED URLS
        let sortType = $(this).data('sort');

        let data = $(this).data('site');

        let rss = [];
        let ghostApi = [];

        data.forEach(item => {
            // Vérifie si l'objet est vide
            if (Object.keys(item).length === 0) {
                return;
            }

            if (item.key.includes("http://") || item.key.includes("https://")) {
                rss.push(item.key);
            } else if (item.key !== "")  {
                ghostApi.push(item);
            }
        });


        let posts = [];

        // Ghost API
        if (ghostApi.length){

            if (ghostApi[0].key.includes("&order")){
                sortType = "none";
            }

            let fetchPromises = ghostApi.map(site => {
        
                return fetch("https://"+site.url.split('/')[2]+"/ghost/api/content/posts/?include=tags&key="+site.key+(site.key.includes("&")?"":"&limit=12&filter=tag:-blog"))
                .then(response => response.json())
                .then(data => {
                    data.posts.forEach(item => {
                        posts.push(formatArticle({
                            tags:           item.tags,
                            duration:       item.og_title,
                            id:             slugify(site.name)+item.id,
                            url:            item.url,
                            title:          item.title,
                            feature_image:  item.feature_image,
                            published_at:   item.published_at,
                            visibility:     item.visibility,
                            feed: {
                                title:      site.name,
                                link:       site.url,
                                avatar:     site.profile_image,
                            }
                        }));
                    });
                })
                .catch(function(e){
                    console.error("Ghost's "+site.name+" API error : "+e)
                });
        
            });
        
            await Promise.all(fetchPromises);
        }

        // RSS
        if (rss.length){
            const rssFeedsUrlsInArray = extractRssFeedUrlFromMultipleWebsite(rss);
            const allFeedsItemsInArray = await getRssFeedFromUrl(rssFeedsUrlsInArray);
            const sortedFeedsItemsInArray = formatAndSortItemsFromRssFeed(allFeedsItemsInArray);

            sortedFeedsItemsInArray.forEach(item => {
                posts.push(formatArticle({
                    tags:           item.categories,
                    data_video:     item.data_video,
                    id:             slugify(item.feed.title+item.title),
                    url:            item.link,
                    title:          item.title,
                    feature_image:  item.image,
                    published_at:   item.pubDate,
                    feed: {
                        title:      item.feed.title,
                        link:       item.feed.link,
                        avatar:     item.feed.avatar,
                    }
                }));
            });

        }
        
        switch (sortType) {
            case 'asc':
                posts.sort((a, b) => new Date(a.isoDate) - new Date(b.isoDate));
                break;
            case 'desc':
                posts.sort((a, b) => new Date(b.isoDate) - new Date(a.isoDate));
                break;
            case 'none':
                break;
            default:
                posts.sort(() => .5 - Math.random());
        }
        

        addtoOwlCarouel(friendscarousel, posts)
    });
}



function addtoOwlCarouel(carousel, posts) {

    // PUSH IN CAROUSEL
    if (carousel.hasClass('owl-loaded')) {
        posts.forEach(function(item) {
            carousel.trigger('add.owl.carousel', [item])
                .trigger('refresh.owl.carousel');
        });
    }
    // PUSH
    else {
        posts.forEach(function(item) {
            carousel.append(item);
        });
        constructShowmoreArticles(carousel);
    }

    carousel.find("time").each(function() {
        doTimeAgo($(this));
    });
    carousel.find('article').each(function() {
        doDataVideo($(this));
    });
}

function getPlaybackPosition(){
    $('article:not(.single-post)').each(function(){
        if ($(this).data('id')){
            let state = playerHistory.get($(this).data('id'));
            if (state){
                $(this).find('.post-media .u-placeholder a').append(`
                <div class="post-media-progress" style="width:`+state.position/state.duration*100+`%" title="`+Math.round(state.position/state.duration*100)+`%"></div>
                `);
                if (state.duration - state.position < 10){
                    $(this).addClass('viewed');
                }
            }
        }
    });
}

function myListActions(){
    if ($('#actions span.fav a').length){

        var state = myList.get($('article').data('id'));

        if (state){
            $('#actions span.fav a').addClass('added');
            $('#actions span.fav a').attr('title','Retirer de ma liste');
        }

        $('#actions span.fav a').click(function(){
            var id =  $(this).closest('article').data('id');
            if ($(this).hasClass('added')){
                myList.delete(id);
                $(this).removeClass('added');
                $(this).attr('title','Ajouter à ma liste');
            }
            else {
                myList.add({'id':id,'time': Date.now()});
                $(this).addClass('added');
                $(this).attr('title','Retirer de ma liste');
            }
            return false;
        });
    }
}

function resizeChat(){
    if ($('iframe.chat').length){
        if ($('.single-post .post-media.post-main').css('display') != 'block'){
            $('iframe.chat').height('100%');
        }
        else {
            $('iframe.chat').height(Math.floor($(window).height() - $('.post-media.post-main .player').height() - $('#gh-head').height()));
        }
    }
}

function fullscreen(){
    $('.single-post .post-media.post-main .fullscreen-button').click(function(){
        if (!document.fullscreenElement) {
            document.querySelector('.single-post .post-media.post-main').requestFullscreen();
        } else {
            document.exitFullscreen();
        }
    });

    $('.gh-post-upgrade-cta a').click(function(){
        document.exitFullscreen();
    });
}

function keyboardNav(){

// enlever la sélection par défaut des liens
$('article:not(.single-post) a').not('[tabindex="0"]').attr('tabindex','-1');
// ajouter la sélection pour les menus
$('.gh-head-brand a, .gh-head-brand button, .gh-head-menu a, .gh-head-menu button, .gh-head-actions a, .gh-head-actions button, .gh-foot-menu a').attr('tabindex','0');
    

$.fn.itemFocus = function() { if ($(this).data('index') !== undefined){
    $('[tabindex="0"].focus').removeClass('focus');
    if (!$(this).parent().is('.active')){
        $(this).closest('.carousel').trigger('to.owl.carousel',[
            $(this).parent().index(),
            0
        ]);
    }
    $(this).addClass('focus').focus();

    let focusItems = JSON.parse(sessionStorage.getItem('FocusItems')) || [];
    let currentUrlEntry = focusItems.find(item => item.url === window.location.href);

    let newEntry = {
        url: window.location.href,
        index: $(this).data('index'),
    };

    if (currentUrlEntry) {
        currentUrlEntry.index = newEntry.index;
    } else {
        focusItems.push(newEntry);
    }

    sessionStorage.setItem('FocusItems', JSON.stringify(focusItems));
}};

$(document).keydown(function(e) {
    const keyCodes = {
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        enter: 13
    };

    if (Object.values(keyCodes).includes(e.keyCode)) {

    if (e.keyCode === keyCodes.enter && $(':focus').length) {
        if ($(':focus').is('article')){
            window.location = $(':focus').find('a').first().attr('href');
        }
    } else {

        let focusableItems = $('[tabindex="0"]:visible');
        focusableItems.each(function(index) {
            $(this).data('index', index);
        });

        let focusItems = JSON.parse(sessionStorage.getItem('FocusItems')) || [];
        let currentUrlEntry = focusItems.find(item => item.url === window.location.href);
        let focusedIndex = currentUrlEntry ? currentUrlEntry.index ? currentUrlEntry.index : $('article[tabindex="0"]').first() ? $('article[tabindex="0"]').first().data('index') : 0 : $('article[tabindex="0"]').first() ? $('article[tabindex="0"]').first().data('index') : 0;
        

        let itemToFocus = focusableItems.filter(function() {
            return $(this).data('index') === focusedIndex;
        }).first();

    if ($('[tabindex="0"].focus').length){

        let itemToFocusCarousel = itemToFocus.closest('.gh-outer') ? itemToFocus.closest('.gh-outer') : null;

        
        let newFocusedIndex = -1;
        if (e.keyCode === keyCodes.right || e.keyCode === keyCodes.down) {
            newFocusedIndex = Math.min(focusedIndex + 1, focusableItems.length - 1);
        } else if (e.keyCode === keyCodes.left || e.keyCode === keyCodes.up) {
            newFocusedIndex = Math.max(focusedIndex - 1, 0); 
        }

        if (newFocusedIndex >= 0){
            itemToFocus = focusableItems.filter(function() {
                return $(this).data('index') === newFocusedIndex;
            }).first();
        }

        if (itemToFocusCarousel && window.matchMedia("(min-width: 768px)").matches){
            if ((e.keyCode === keyCodes.up && itemToFocusCarousel.prev().find('[tabindex="0"]').length > 0) 
                || (e.keyCode === keyCodes.down && itemToFocusCarousel.next().find('[tabindex="0"]').length > 0)) {
                itemToFocus = (e.keyCode === keyCodes.up ? itemToFocusCarousel.prev().find('[tabindex="0"]').first() : itemToFocusCarousel.next().find('[tabindex="0"]').first());
            }
        }
    
    }
        
        itemToFocus.itemFocus();

        e.preventDefault();
    }
    } 

});



}


function loadPlaylist(){ 

// PLAYLIST
if ($('body').hasClass("post-template") && $('.playlist').length){

    const playlistCarousel = $('.playlist');

    // VERIFIER SI Y'A PLUS D'UN ARTICLE
    if (playlistCarousel.find('article').length > 1){
        playlist(playlistCarousel);
    }
    // SINON SUPPRIMER LA PLAYLIST
    else{ 
        playlistCarousel.closest('.gh-outer').remove();
    }
}

function playlist(playlistCarousel){

    // FIND CURRENT ARTICLE PLAYED
    if (!playlistCarousel.find('article.current').length){
        playlistCarousel.find('article[data-id="'+$('.single-post').data('id')+'"]').addClass('current');
    }
    playlistCarousel.trigger('to.owl.carousel',[
        playlistCarousel.find('article.current').parent().index(),
        0
    ]);

    // FIND NEXT VIDEO
    const nextVideo =   playlistCarousel.hasClass('owl-loaded') ?
                        playlistCarousel.find('article.current').parent().next().find('article'):
                        playlistCarousel.find('article.current').next();

    if (nextVideo.length && $('body').hasClass("videaste_player")){
        var autoplay = localStorage['autoplay'] == 'false' ? false : true;
        // Create next button
        document.querySelector('.single-post .post-media.post-main .js-reframe').insertAdjacentHTML('beforeend',
        `<div class="nextbutton">
            <div class="autoplay">Lecture automatique <label class="switch"><input type="checkbox" `+ (autoplay ? 'checked' : '') +`><span class="slider round"></span></label></div>
            <div class="nextvideo"></div>
        </div>`);
        // Add article in next button
        $('.single-post .post-media.post-main .nextvideo').append(nextVideo.clone());

        $('.autoplay input').change(function() {
            localStorage['autoplay'] = this.checked ? true : false;
        });
    }
}

// SUGGESTIONS
if ($('body').hasClass("post-template") && $('.suggest').length){

    fetch('/videos/carousel/')
        .then(response => {
            if (!response.ok) {
                throw new Error("HTTP error " + response.status);
            }
            return response.text();
        })
        .then(data => {
            if (data.length){
                var parser = new DOMParser();
                var doc = parser.parseFromString(data, 'text/html');
                var articles = doc.getElementsByTagName('article');
                let posts = [];

                var LoadedArticlesID = [];
                LoadedArticlesID.push($('.single-post').data('id'));
                $('.playlist article').each(function(){
                    LoadedArticlesID.push($(this).data('id'));
                });

                for(let article of articles) {
                    if (!LoadedArticlesID.includes(article.getAttribute('data-id'))){
                        posts.push(article);
                    }
                }
                
                posts.sort(() => .5 - Math.random());
                addtoOwlCarouel($('.suggest'), posts.slice(0, 12));
                carousel();
                getPlaybackPosition();
            }
        })
        .catch(function () {
            this.dataError = true;
        })
    }

}




/**
 * HTML format of article
 *  item.tags
 *  item.duration
 *  item.id
 *  item.url
 *  item.title
 *  item.feature_image
 *  item.published_at

 *  item.feed.title
 *  item.feed.link
 *  item.feed.avatar
**/
function formatArticle(item){

    // TAGS
    var tags = '';
    if (item.tags[0] !== undefined && item.tags[0].hasOwnProperty("slug")){
        item.tags.forEach((tag) => {
            tags += 'tag-'+slugify(tag.slug)+' ';
        });
    } else {
        item.tags.forEach((tag) => {
            // SI AUTRE SYSTEME DE TAG, ON TRICHE LES TAGS POUR DIRECT
            if (/^en direct$|^direct$|^live$/i.test(tag)) {
                tags += "tag-hash-live ";
            }
            // SI AUTRE SYSTEME DE TAG, ON TRICHE LES TAGS POUR AVANT-PREMIERE
            else if (/^avant-première$|^avant-premiere$|^premiere$|^preview$/i.test(tag)) {
                tags += "tag-hash-preview ";
            }
            else {
                tags += 'tag-'+slugify(tag)+' ';
            }
        });
    }

    // ID
    var id = item.id ? item.id : slugify(item.title);

    // PLAYBACK
    var playback = ''; var viewed = '';
    var state = playerHistory.add(item.id);
    if (state){
        playback = state.position/state.duration*100;
        if (state.position/state.duration*100 > 80){
            viewed = 'viewed';
        }
    }

    // TARGET
    var target = item.feed.title != '' ? ' target="_blank"' : '';

    // CURRENT
    var current = '';
    if ($('.single-post').data('id') == item.id){
        current = 'current';
    }

    // ARTICLE
    let article = `<article class="post ${tags} ${viewed} ${current} featured" data-duration="${item.duration?item.duration:item.data_video?item.data_video:''}" data-id="${id}" tabindex="0">
                        <figure class="post-media">
                            <div class="u-placeholder square">
                                <a class="post-image-link" ${target} href="${item.url}" title="${item.title}" tabindex="-1">
                                    <img class="post-image u-object-fit" src="${item.feature_image ? item.feature_image : 'https://via.placeholder.com/1024x576.png?text=Vignette+absente'}" alt="${item.link}" loading="lazy">`;
    if (playback != ''){
        article += `<div class="post-media-progress" style="width:`+playback+`%" title="`+Math.round(playback)+`%"></div>`;
    }
    if (item.visibility !== "" && item.visibility !== "public"){
        article += `<span class="post-media-access" title="&#x27A1; ${item.feed.title}">`;
        if (item.visibility == "paid") { article += ` Membre`; }
        else if (item.visibility == "members") { article += ` Abonné⸱e`; }
        article += `</span>`;
    }
    else if (item.feed.title){
        article += `<span class="post-media-access" title="&#x27A1; ${item.feed.title}"></span>`;
    }
        article += `</a>
                            </div>
                        </figure>
                        <header class="post-header">
                            <h2 class="post-title">`;
    if (item.feed.title){
        article += `<a href="${item.feed.link}" target="_blank" title="${item.feed.title}" tabindex="-1"><img class="avatar" src="${item.feed.avatar}"></a>`;
    }
        article += `<a class="post-title-link" ${target} href="${item.url}" title="${item.title}" tabindex="-1">${item.title}</a>
                            </h2>
                            <div class="post-meta">
                                <span class="post-meta-item post-meta-date">
                                    <time datetime="${moment(item.published_at).format("YYYY-MM-DD HH:mm")}">${moment(item.published_at).format("DD MMM YYYY")}</time>
                                </span>`;
    if (item.feed.title){
        article += `<span class="author-link"><a href="${item.feed.link}" target="_blank" tabindex="-1">${item.feed.title}</a></span>`;
    }
        article += `</div>
                            </header>
                    </article>`;
    return article;
}

function modal(){
    var urlHash = window.location.hash;
    if (urlHash.startsWith("#/modal/")) {
        var modalHash = urlHash.substring(8);
        var url = $('a[data-hash="'+modalHash+'"]').first().attr('href');

        if (url !== undefined){
        if ($("#modal iframe").attr('src') !== url){
            $("#modal iframe").attr('src',url);
        }
        MicroModal.show('modal',{
            onShow: modal => modal_onShow(),
            onClose: modal => modal_onClose(),
            disableScroll: true
        });
        }
    }
}
function modal_Construct(){
    $('a[href$="#modal"]').each(function(){
        var url = $(this).attr('href').replace(/#modal$/, "");
        var hash = slugify($(this).text());
        $(this).attr('href',url);
        $(this).attr('data-hash',hash);
        $(this).click(function(e){
            e.preventDefault();
            window.location.hash = "/modal/"+hash;
        });
    });
}
function modal_onShow(){

}
function modal_onClose(){
    history.pushState(null, null, ' ');
}



function removeUnaccessDom(){
    const memberTypes = ['free', 'member', 'memberpaid'];

    let selectors = [];

    memberTypes.forEach((memberType) => {
        const otherTypes = memberTypes.filter(type => type !== memberType);
        
        const notClasses =  [
            `showto-${memberType}`,
            `block-showto-${memberType}`,
            `flex-showto-${memberType}`
        ];
        
        const classesToHide = otherTypes.map(type => [
            `showto-${type}`,
            `block-showto-${type}`,
            `flex-showto-${type}`
        ]).flat();
        
        classesToHide.forEach(classToHide => {
            const notSelectors = notClasses.map(notClass => `:not(.${notClass})`).join('');
            selectors.push(`.useris-${memberType} .${classToHide}${notSelectors}`);
        });
    });

    let elements = document.querySelectorAll(selectors.join(', '));
    elements.forEach((element) => {
        element.remove();
    });
}