// CUSTOM-HISTORY.HBS
var articles = document.querySelectorAll('article');
articles.forEach(function(article) {

  let state = playerHistory.get(article.dataset.id);

  if (!state){
    article.remove();
  }
  else {

    var button = document.createElement('div');
    button.classList.add('remove-button');
    button.setAttribute('title', 'Retirer de mon historique');
    button.innerHTML = '×';
    var placeholder = article.querySelector('.post-media .u-placeholder');
    if (placeholder) {
    placeholder.classList.add('is-remove-button');
    placeholder.appendChild(button);
    }

    var stateFirst = playerHistory.get(document.querySelector('article:first-of-type').dataset.id);
    if (state.time > stateFirst.time){
      document.querySelector('.post-feed').prepend(article);
    }
  }
});

var historyInfo = document.createElement('div');
document.querySelector('.term-description').append(historyInfo);

if (document.querySelectorAll('article').length == 0){
  historyInfo.innerHTML = 'Votre historique est vide.';
}
else {

  var buttonAll = document.createElement('a');
    buttonAll.setAttribute('href', '#');
    buttonAll.innerHTML = 'Effacer tout mon historique.';
    buttonAll.addEventListener('click', function(e) {
      e.preventDefault();
      if (confirm("Merci de confirmer la suppression de tout votre historique.")){
        playerHistory.deleteAll();
        document.querySelectorAll('.post-feed article').forEach(function(node) {
            node.remove();
        });
        historyInfo.innerHTML = 'Votre historique est vide.';
      }
    });

  historyInfo.append(buttonAll);
  document.querySelector('.post-feed').style.display = 'block';
}

document.querySelectorAll('.remove-button').forEach(function(button) {
  button.addEventListener('click', function() {
    if (confirm("Merci de confirmer la suppression de cette vidéo de votre historique.")){
        var article = this.closest('article');
        playerHistory.delete(article.dataset.id);
        article.remove();
    }
  });
});