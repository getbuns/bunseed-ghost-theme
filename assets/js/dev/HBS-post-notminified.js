// POST.HBS
const videoHosts = ['youtube', 'peertube', 'streamlike', 'vimeo']
const chatHosts = ['livechat', 'webchat', 'sli.do']

// On sélectionne les iframe du contenu du post
var iframes = document.querySelectorAll('.gh-content iframe')

// Si plusieurs iframes, on supprime la première qui est un teaser
if (iframes.length > 1 && !chatHosts.some(host => iframes[1].src.includes(host))) {
  if (videoHosts.some(host => iframes[0].src.includes(host))) {
    iframes[0].remove()
  }
}
// On resélectionne les iframes vu qu'il y en a une en moins
iframes = document.querySelectorAll('.gh-content iframe')

// Si y'a une iframe
if (iframes.length > 0) {

  const postMedia = document.querySelector('.post-media.post-main')

  // Si y'a un chat
  if (iframes[1] && chatHosts.some(host => iframes[1].src.includes(host))) {
    var iframe = iframes[1]
    document.querySelector("body").classList.add("videaste_chat")
    iframe.classList.add("chat")
    iframe.style.height = "100%"
    postMedia.querySelector(".post-image").classList.add("player")
    postMedia.appendChild(iframe)
  }

  // Si y'a un player
  if (videoHosts.some(host => iframes[0].src.includes(host))) {
    var iframe = iframes[0];
    document.querySelector("body").classList.add("videaste_player")
    // iframe.style.height = "100%"
    iframe.classList.add("player")
    iframe.setAttribute("id","player")
    
    // activer API
    var url = iframe.getAttribute('src').split('#')[0]
    url += (url.indexOf('?') === -1 ? '?' : '&') + 'api=1&enablejsapi=1&events=1&autoplay=1'
    url += window.location.hostname !== "localhost" ? '&origin='+window.location.origin : ""
    iframe.setAttribute('src', url)
    // autoriser autoplay
    iframe.setAttribute('allow', 'autoplay')
    iframe.setAttribute('allow-top-navigation', '')
    iframe.getAttribute('sandbox')?iframe.setAttribute('sandbox', iframe.getAttribute('sandbox')+' allow-top-navigation'):''
    

    // MOVE PLAYER TO HEAD
    postMedia.querySelector(".post-image").remove();
    postMedia.prepend(iframe)

    iframe.style.position = "relative";
    if (iframe.getAttribute("height") !== undefined && iframe.getAttribute("width") !== undefined){
        iframe.style.height = (iframe.offsetWidth * iframe.getAttribute("height") / iframe.getAttribute("width"))+"px";
    }
    else {
        iframe.style.height = (iframe.offsetWidth * .5625)+"px";
    }
    if (document.readyState === "loading") {
        document.addEventListener("DOMContentLoaded", function() {
            playerReframe(iframe)
        })
    } else {
        playerReframe(iframe)
    }

    // REFRAME PLAYER + ADD FULLSCREEN BUTTON 
    function playerReframe(iframe){
        if (typeof reframe === "function"){
            reframe(iframe)
              const fullscreenButton = document.createElement('div')
              fullscreenButton.classList.add('fullscreen-button')
            iframe.parentNode.appendChild(fullscreenButton)

            // RUN API
            run(iframe)
        }
        else {
          setTimeout(function() { playerReframe(iframe) }, 100);
        }
    }
    
  }

}

// Afficher le block d'inscription
function upgradeBlock(action){

  const placeholder = document.querySelector('.single-post .post-media')
  const upgradeBlock = document.querySelector('aside.gh-post-upgrade-cta')

  if (upgradeBlock !== undefined){
    if (action == 'show' && upgradeBlock.parentNode !== placeholder){
      placeholder.classList.add('calltoaction')
      placeholder.prepend(upgradeBlock)
    }
    else if (action == 'hide' && upgradeBlock.parentNode == placeholder){
      placeholder.classList.remove('calltoaction')
      document.querySelector('.gh-content').appendChild(upgradeBlock)
    }
  }
}

if (document.querySelector('.single-post').classList.contains('noaccess') && !document.querySelector('body').classList.contains('videaste_player')){
  upgradeBlock('show')
}


async function run(videoiFrame) {

  // YOUTUBE
  if (videoiFrame.src.includes('youtube.com')){

    var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  }

  // VIMEO
  else if (videoiFrame.src.includes('vimeo.com')){

    loadScript("https://player.vimeo.com/api/player.js", function(){
        var player = new Vimeo.Player(videoiFrame);
        
        var state = playerHistory.get('{{id}}')
        if (state){
            if (state.duration - state.position > 10){ // Si c'est la fin de la video, lire depuis le début
                player.setCurrentTime(state.position - 5) // 5 sec en arriere
            }
        }

        // timecodes cliquables
        useTimecodes("vimeo",player);

        player.on('timeupdate', function(data) {
            player_playing(data.seconds,data.duration)
        });

    });

  }

  // STREAMLIKE
  else if (videoiFrame.src.includes('streamlike.com')){

        var duration = false;
        var urlObjet = new URL(videoiFrame.src);
        var med_id = urlObjet.searchParams.get('med_id');
        fetch('https://cdn.streamlike.com/ws/media?media_id=' + med_id + '&f=json')
            .then(response => response.json())
            .then(data => {
                duration = data.media.metadata.global.duration;
            })
            .catch((error) => console.error('Erreur:', error));

        var state = playerHistory.get('{{id}}')
        if (state){
            if (state.duration - state.position > 10){ // Si c'est la fin de la video, lire depuis le début
                videoiFrame.addEventListener('load', function() {
                    videoiFrame.contentWindow.postMessage('["seek",'+ (state.position - 5) +']', "*")
                }, false);

            }
        }

        // timecodes cliquables
        useTimecodes("streamlike",videoiFrame);
        
        window.addEventListener("message", function(e){
            if (typeof e.data === 'string' || e.data instanceof String){
                sl = JSON.parse(e.data)
                if (sl[0] == "sl-progress" && duration !== false){
                    player_playing(sl[1],duration)
                }
            }
        });

  }

  // PEERTUBE
  else { 

    const PeerTubePlayer = window['PeerTubePlayer']
    const player = new PeerTubePlayer(videoiFrame)

    await player.ready

    var state = playerHistory.get('{{id}}')
    if (state){
        if (state.duration - state.position > 10){ // Si c'est la fin de la video, lire depuis le début
            await player.seek(state.position - 5) // 5 sec en arriere
        }
    }

    // timecodes cliquables
    useTimecodes("peertube",player);
    
    await player.addEventListener("playbackStatusUpdate", function(e){

        if (e.playbackState == "playing"){
            player_playing(e.position,e.duration)
        }

    })

  }
  
}

// This function creates a YouTube player instance using the existing iframe.
var player;
function onYouTubeIframeAPIReady() {
    if (document.querySelector("#player").src.includes('youtube.com')){
        player = new YT.Player('player', {
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
    }
}

// The API will call this function when the video player is ready.
function onPlayerReady(event) {
    var state = playerHistory.get('{{id}}')
    if (state){
        if (state.duration - state.position > 10){ // Si c'est la fin de la video, lire depuis le début
          player.seekTo(state.position - 5, true);
          player.playVideo();
        }
    }

    // timecodes cliquables
    useTimecodes("youtube",player);
  
}

// The API calls this function when the player's state changes.
function onPlayerStateChange(event) {

    player_playing(player.getCurrentTime(),player.getDuration())

    if (event.data === YT.PlayerState.PLAYING) {
          timer = setInterval(function() {
            player_playing(player.getCurrentTime(),player.getDuration())
          }, 1000);
        }
    else {
        if (typeof timer !== 'undefined') {
            clearInterval(timer);
        }
    }

}



function player_playing(position,duration){

  if (duration > 20 && position > 5
      && !document.querySelector('.single-post').classList.contains('noaccess')){

            // Enregistrer position de lecture
            var state = {'id':'{{id}}', 'position': position, 'duration': duration, 'time': Date.now()}
            playerHistory.add(state);

              // Next video bouton
            if (duration - position < 16){
                document.querySelector('.single-post .post-media.post-main').classList.add("display-nextbutton")
            } else {
                document.querySelector('.single-post .post-media.post-main').classList.remove("display-nextbutton")
            }
            if (duration - position < 1 && localStorage['autoplay'] != 'false'){
                if (document.querySelector(".nextbutton") !== null){
                    window.location = document.querySelector(".nextbutton a.post-image-link").getAttribute("href")
                }
            }
        }
  // Afficher call to action si fin de vidéo teaser et pas accès
  else if (duration > 10 && document.querySelector('.single-post').classList.contains('noaccess')){

    if (duration - position < 1) {
      upgradeBlock('show')
    } else {
      upgradeBlock('hide')
    }

  }

}

function loadScript(url, callback){
        var script = document.createElement("script")
        script.type = "text/javascript";
    
        if (script.readyState){  //IE
            script.onreadystatechange = function(){
                if (script.readyState === "loaded" || script.readyState === "complete"){
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function(){
                callback();
            };
        }
    
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
}


function useTimecodes(source,player){

    function timecodeToSeconds(timecode) {
        var parts = timecode.split(':').map(Number);
        var seconds = 0;

        if (parts.length === 3) {
            seconds = parts[0] * 3600 + parts[1] * 60 + parts[2];
        } else if (parts.length === 2) {
            seconds = parts[0] * 60 + parts[1];
        }

        return seconds;
    }

    // Créer les liens cliquables s'il y a des timecodes dans des paragraphes
    document.querySelectorAll('.gh-content p').forEach(function(p) {
        p.innerHTML = p.innerHTML.replace(
            /\b((?:\d{1,2}:)?\d{1,2}:\d{2})\b/g,
            function(match) {
                var seconds = timecodeToSeconds(match);
                return `<a href="javascript:;" class="gototimecode" data-seconds="${seconds}">${match}</a>`;
            }
        );
    });

    // Rendre les liens actifs suivant le player
    document.addEventListener('click', function(e) {
        if (e.target && e.target.matches('.gototimecode')) {
            e.preventDefault();
            var seconds = e.target.getAttribute('data-seconds');
            if (seconds && player) {

                if (source == "youtube"){
                    player.seekTo(parseFloat(seconds));
                    player.playVideo();
                }
                else if (source == "peertube"){
                    player.seek(parseFloat(seconds));
                    player.play();
                }
                else if (source == "vimeo"){
                    player.setCurrentTime(parseFloat(seconds));
                    player.play();
                }
                else if (source == "streamlike"){
                    player.contentWindow.postMessage('["seek",'+ parseFloat(seconds) +']', "*");
                    player.contentWindow.postMessage('["play"]', "*");
                }

                if (document.querySelector('html').getAttribute('data-scroll') > 145){
                    document.getElementById('player').scrollIntoView({ behavior: 'smooth' });
                }

            }
        }
    });

}