

function getPaidContent(){ if (typeof document.querySelector('article.single-post').dataset.video !== 'undefined') {

    const postData = convertFormatedStringToObject(document.querySelector('article.single-post').dataset.video);

    if (
        (typeof postData.rent !== 'undefined' || typeof postData.buy !== 'undefined')
        && document.querySelector('article.single-post')
        && document.querySelector('aside.gh-post-upgrade-cta')
    ){

        // IN ALL CASES, CREATE BUTTONS
        createButtonsBuyRent(postData);

        // IF MEMBER IS LOGGED IN, CHECK IF PAID
        if (document.querySelector('body').dataset.memberid){

            // Check Stripe payment
            fetch('https://pay.'+hostname+'/access'
                + '?postid='+document.querySelector('article.single-post').dataset.id
                + '&memberid='+document.querySelector('body').dataset.memberid
                )
            .then(response => {
                if (!response.ok) {
                    throw new Error("HTTP error " + response.status);
                }
                return response.json();
            })
            .then(json => {

                // this.duration
                // this.paymentdate

                // If paid, display content
                if (this.html){

                    document.querySelector('article.single-post .gh-content').innerHTML(this.html);

                    // Launch move player to header
                    // En faire une fonction appelable
                    // Ou récupérer dès le this.html le player et le bouger direct

                }

                // this.users = json;
                // console.log(this.users);

            })
            .catch(function () {
                this.dataError = true;
            })

        }

    }

}}


function createButtonsBuyRent(postData){

    // Button RENT
    if (postData.rent){

        const rentData = postData.rent.split(',');
        const price = rentData[0];
        const amount = rentData[1] ? rentData[1] : '';
        const duration = rentData[2] ? rentData[2] : '';

        const   buyButton = document.createElement('a');
                buyButton.classList.add('gh-btn');
                buyButton.style.color = '#ff1a75';
                buyButton.href  = 'https://pay.'+hostname
                                + '/buy?price='+price
                                + '&url='+encodeURIComponent(window.location.href)
                                + '&memberid=' + (localStorage['memberid'] ? localStorage['memberid'] : document.querySelector('body').dataset.memberid)
                                + '&memberemail=' + (localStorage['memberemail'] ? localStorage['memberemail'] : document.querySelector('body').dataset.memberemail)
                buyButton.text = 'Louer ('+duration+') '+ (amount == "free" ? "à prix libre" : amount);

        document.querySelector('aside.gh-post-upgrade-cta .gh-btn').after(buyButton);

    }

    // Button BUY
    if (postData.buy){

        const buyData = postData.buy.split(',');
        const price = buyData[0];
        const amount = buyData[1] ? buyData[1] : '';

        const   buyButton = document.createElement('a');
                buyButton.classList.add('gh-btn');
                buyButton.style.color = '#ff1a75';
                buyButton.href  = 'https://pay.'+hostname
                                + '/buy?price='+price
                                + '&url='+encodeURIComponent(window.location.href)
                                + '&memberid=' + (localStorage['memberid'] ? localStorage['memberid'] : document.querySelector('body').dataset.memberid)
                                + '&memberemail=' + (localStorage['memberemail'] ? localStorage['memberemail'] : document.querySelector('body').dataset.memberemail)
                buyButton.text = 'Acheter '+ (amount == "free" ? "à prix libre" : amount);

        document.querySelector('aside.gh-post-upgrade-cta .gh-btn').after(buyButton);

    }

}

function ProductsCards(){

    $('.kg-product-card-button.kg-product-card-btn-accent').each(function(){

        const href = $(this).attr('href');

        if (href.includes("#book=") || href.includes("#price=")){

            const price = href.split('book=')[1];

            const newhref   = 'https://pay.'+hostname+'/pay/'
                            + '?price=' + price
                            + '&type=' + (href.includes("#book=") ? 'book' : 'auto')
                            + '&url='+encodeURIComponent(window.location.href.split('?')[0])
                            + '&memberid=' + (localStorage['memberid'] ? localStorage['memberid'] : document.querySelector('body').dataset.memberid)
                            + '&memberemail=' + (localStorage['memberemail'] ? localStorage['memberemail'] : document.querySelector('body').dataset.memberemail)

            // Récupérer le nombre de places restantes
            if (href.includes('#book=')){

                // toutes les minutes
                // setInterval( function() {}, 60*1000 );

                    fetch('https://pay.'+hostname+'/product/?price='+price)

                    .then(response => {
                        if (!response.ok) {
                            throw new Error("HTTP error " + response.status);
                        }
                        return response.json();
                    })
                    .then(data => {

                        const quantityleft = Math.max(0, Number(data.quantity) - Number(data.quantitypaid) );

                        // Afficher le nombre de places restantes
                        $(this).prev('.kg-product-card-description').find('p').append(
                            '<u>'+quantityleft+' place'+(quantityleft<2?'':'s')+' restante'+(quantityleft<2?'':'s')+'</u>'
                        );
                        
                        // Désactiver le produit s'il n'y a plus de places
                        if (quantityleft == 0){
                            $(this).removeAttr('href');
                            $(this).closest('.kg-card.kg-product-card').css({
                                'pointer-events':'none',
                                'opacity':'.6',
                            });
                        }
                        else {
                            $(this).attr('href',newhref);
                            $(this).attr('target','_parent');
                        }

                    })
                    .catch(function () {
                        this.dataError = true;
                    })

            }
            else {
                $(this).attr('href',newhref);
                $(this).attr('target','_parent');
            }

            // PAIEMENT SUCCES MESSAGE
            if (_GET('price') && _GET('price') == price && _GET('paymentsuccess')){

                $(this).removeAttr('href');
                $(this).closest('.kg-card.kg-product-card').css({
                    'pointer-events':'none',
                });

                if (href.includes('#book=')){
                    popInfo('Merci pour votre réservation. Merci de vérifier votre boite email.');
                    $(this).text('Merci pour votre réservation !');
                }
                else {
                    popInfo('Merci pour votre achat. Merci de vérifier votre boite email.');
                    $(this).text('Merci pour votre achat !');
                }
  
            }

        }
    });

}

// A MODIFIER !
function popInfo(text){
    alert(text);
}

function _GET(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if (pair[0] == variable) {
        return pair[1];
      }
    }
    return false;
}