function carousel() {

    var carouselPosts = $('.carousel').not('.authors').not('.playlist');
    var carouselPlaylist = $('.carousel.playlist');
    var carouselAuthors = $('.carousel.authors');

    var imageHeight, nav;

    function moveNav(target) {
        imageHeight = target.find('.post-image').height();
        nav = target.find('.owl-prev, .owl-next');
        nav.css({
            top: imageHeight / 2 + 'px',
            opacity: 1,
        });
    }

    if (window.matchMedia("(min-width: 768px)").matches) {

        carouselPosts.owlCarousel({
            dots: false,
            margin: 12,
            nav: true,
            smartSpeed: 60,
            lazyLoad: true,
            navText: [
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M20.547 22.107l-6.107-6.107 6.107-6.12-1.88-1.88-8 8 8 8 1.88-1.893z"></path></svg>',
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M11.453 22.107l6.107-6.107-6.107-6.12 1.88-1.88 8 8-8 8-1.88-1.893z"></path></svg>',
            ],
            onInitialized: function() {
                moveNav(carouselPosts);
                carouselPosts.each(function(){
                    destroyShowmoreArticles($(this));
                });
            },
            onResized: function() {
                moveNav(carouselPosts);
                if (window.matchMedia("(max-width: 767px)").matches) {
                    carouselPosts.trigger('destroy.owl.carousel');
                    carouselPosts.each(function(){
                        constructShowmoreArticles($(this));
                    });
                }
            },
            onRefreshed: function() {
                moveNav(carouselPosts);
            },
            responsive: {
                0: {
                    items: 1,
                    slideBy: 1,
                    nav: true,
                },
                768: {
                    items: 3,
                    slideBy: 3,
                },
                992: {
                    items: 4,
                    slideBy: 4,
                },
            },
        });
    }
    else {
        carouselPosts.each(function(){
            constructShowmoreArticles($(this));
        });
    }

    carouselPlaylist.owlCarousel({
        dots: false,
        margin: 12,
        nav: true,
        smartSpeed: 60,
        lazyLoad: true,
        navText: [
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M20.547 22.107l-6.107-6.107 6.107-6.12-1.88-1.88-8 8 8 8 1.88-1.893z"></path></svg>',
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M11.453 22.107l6.107-6.107-6.107-6.12 1.88-1.88 8 8-8 8-1.88-1.893z"></path></svg>',
        ],
        onInitialized: function() {
            moveNav(carouselPlaylist);
        },
        onResized: function() {
            moveNav(carouselPlaylist);
        },
        onRefreshed: function() {
            moveNav(carouselPlaylist);
        },
        responsive: {
            0: {
                items: 2,
                slideBy: 2,
                nav: true,
            },
            768: {
                items: 3,
                slideBy: 3,
            },
            992: {
                items: 4,
                slideBy: 4,
            },
        },
    });

    carouselAuthors.owlCarousel({
        dots: false,
        margin: 24,
        nav: true,
        smartSpeed: 60,
        lazyLoad: true,
        navText: [
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M20.547 22.107l-6.107-6.107 6.107-6.12-1.88-1.88-8 8 8 8 1.88-1.893z"></path></svg>',
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" fill="currentColor"><path d="M11.453 22.107l6.107-6.107-6.107-6.12 1.88-1.88 8 8-8 8-1.88-1.893z"></path></svg>',
        ],
        onInitialized: function() {
            moveNav(carouselAuthors);
            carouselAuthors.css('visibility', 'visible');
        },
        onResized: function() {
            moveNav(carouselAuthors);
        },
        onRefreshed: function() {
            moveNav(carouselAuthors);
        },
        responsive: {
            0: {
                items: 3,
                slideBy: 3,
                nav: true,
                margin: 0,
            },
            768: {
                items: 4,
                slideBy: 4,
                margin: 12,
            },
            992: {
                items: 5,
                slideBy: 5,
            },
        },
    });
}


















































// PLAYLIST
if (typeof api === 'object' && $('.playlist').data('tag')){
    api.posts.browse({
        filter: 'tag:'+playlistCarousel.data('tag'),
        order: 'published_at ASC',
        include: 'tags'
    })
    .then((articles) => {
    
        let posts = [];
        articles.forEach((item) => {
            posts.push(formatArticle({
                tags:           item.tags,
                data_video:     item.codeinjection_foot,
                id:             item.id,
                url:            item.url,
                title:          item.title,
                feature_image:  item.feature_image,
                published_at:   item.published_at,
                feed: {
                    title:      '',
                    link:       '',
                    avatar:     '',
                }
            }));
        });
    
        addtoOwlCarouel(playlistCarousel, posts);

        playlist(playlistCarousel);
    })
    .catch((err) => {
        console.error(err);
    });
}
else {
    playlist(playlistCarousel);
}

// SUGGESTIONS
if (typeof api === 'object' && $('body').hasClass("post-template") && $('.suggest').length){

    api.posts.browse({
        filter: 'id:-'+$('.single-post').data('id'),
        include: 'tags',
        limit: 12
    })
    .then((articles) => {
        
        let posts = [];
        articles.forEach((item) => {
            posts.push(formatArticle({
                tags:           item.tags,
                data_video:     item.codeinjection_foot,
                id:             item.id,
                url:            item.url,
                title:          item.title,
                feature_image:  item.feature_image,
                published_at:   item.published_at,
                feed: {
                    title:      '',
                    link:       '',
                    avatar:     '',
                }
            }));
        });

        posts.sort(() => .5 - Math.random());
        addtoOwlCarouel($('.suggest'), posts);
        carousel();
    })
    .catch((err) => {
        console.error(err);
    });
}