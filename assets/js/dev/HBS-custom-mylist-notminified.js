// CUSTOM-MYLIST.HBS
var articles = document.querySelectorAll('article');
articles.forEach(function(article) {

  var state = myList.get(article.dataset.id);
  if (!state){
    article.remove();
  }
  else {

    var button = document.createElement('div');
    button.classList.add('remove-button');
    button.setAttribute('title', 'Retirer de ma liste');
    button.innerHTML = '×';
    var placeholder = article.querySelector('.post-media .u-placeholder');
    if (placeholder) {
      placeholder.classList.add('is-remove-button');
      placeholder.appendChild(button);
    }

    var stateFirst = myList.get(document.querySelector('article:first-of-type').dataset.id);
    if (state.time > stateFirst.time){
      document.querySelector('.post-feed').prepend(article);
    }
  }
});

if (document.querySelectorAll('article').length == 0){
  document.querySelector('.term-description').append('Aucune vidéo présente dans votre liste pour le moment.');
}
else {
  document.querySelector('.post-feed').style.display = 'block';
}

document.querySelectorAll('.remove-button').forEach(function(button) {
button.addEventListener('click', function() {
  if (confirm("Merci de confirmer la suppression de cette vidéo de votre liste.")){
    var article = this.closest('article');
    myList.delete(article.dataset.id);
    article.remove();
  }
});
});