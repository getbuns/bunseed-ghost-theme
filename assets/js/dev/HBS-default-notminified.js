// DEFAULT.HBS
if (localStorage.getItem('alto_dark') == 'true') {
    document.documentElement.classList.add('dark-mode');
}

const debounce = (fn) => {
    let frame;
    return (...params) => {
      if (frame) { 
        cancelAnimationFrame(frame);
      }
      frame = requestAnimationFrame(() => {
        fn(...params);
      });
    } 
  };
  const storeScroll = () => {
    document.documentElement.dataset.scroll = window.scrollY;
  }
  document.addEventListener('scroll', debounce(storeScroll), { passive: true });
  storeScroll();


class LocalStorageList {
    constructor(storageKey) {
        this.storageKey = storageKey;
        this.history = JSON.parse(localStorage.getItem(storageKey)) || [];
    }

    add(data) {
        let existingIndex = this.history.findIndex(post => post.id === data.id);
        if (existingIndex > -1) {
            this.history[existingIndex] = data;
        } else {
            this.history.push(data);
        }
        this.syncWithLocalStorage();
    }

    get(postid) {
        let post = this.history.find(post => post.id === postid);
        return post || null;
    }

    delete(postid) {
        this.history = this.history.filter(post => post.id !== postid);
        this.syncWithLocalStorage();
    }

    deleteAll() {
        this.history = [];
        this.syncWithLocalStorage();
    }

    syncWithLocalStorage() {
        localStorage.setItem(this.storageKey, JSON.stringify(this.history));
    }
}

class PlayerHistory extends LocalStorageList {
    constructor() {
        super("PlayerHistory"); // call the super class constructor and pass in the name argument
        if (PlayerHistory.instance) { return PlayerHistory.instance; }
        PlayerHistory.instance = this;
    }
}

let playerHistory = new PlayerHistory();


class MyList extends LocalStorageList {
    constructor() {
        super("MyList"); // call the super class constructor and pass in the name argument
        if (MyList.instance) { return MyList.instance; }
        MyList.instance = this;
    }
}

let myList = new MyList();



/* FOOTER */

const btn = document.querySelector(".nightshift-toggle");
const prefersColorScheme = window.matchMedia("(prefers-color-scheme: dark)");
let preferMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
const chosenMode = localStorage.getItem("theme");
const htmlEl = document.querySelector("html");

// Fonction pour basculer le thème
function toggleTheme(current) {
    if (current === "dark") {
        htmlEl.classList.remove("dark-mode");
        btn.classList.remove("on");
        if (preferMode == "light"){
            localStorage.removeItem("theme");
        } else {
            localStorage.setItem("theme", "light");
            htmlEl.classList.add("light-mode");
        }
        return "light";
    } else {
        htmlEl.classList.remove("light-mode");
        btn.classList.add("on");
        if (preferMode == "dark"){
            localStorage.removeItem("theme");
        } else {
            localStorage.setItem("theme", "dark");
            htmlEl.classList.add("dark-mode");
        }
        return "dark";
    }
}

// Récupérer le thème actuel et l'appliquer
let currentTheme = chosenMode ? chosenMode : preferMode;
toggleTheme(currentTheme == "dark" ? "light" : "dark");

btn.addEventListener("click", function () {
    currentTheme = toggleTheme(currentTheme);
});
prefersColorScheme.addEventListener("change", function(){
    preferMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
    currentTheme = toggleTheme(preferMode == "dark" ? "light" : "dark")
});