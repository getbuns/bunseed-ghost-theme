# video-maker-ghost-theme - Thème Ghost pour vidéastes

Thème Ghost du projet [BUNSEED](https://bunseed.org/). 

Ce thème peut être utilisé seul sur une instance [Ghost](https://github.com/TryGhost/Ghost) classique ou en tant que thème avec les outils du projet [BUNSEED](https://bunseed.org/). 

* Pour installer le thème : [Guide Vidéaste](https://docs.bunseed.org/docs/installation-videaste/intro)
* Pour configurer/utiliser le thème : [Guide Vidéaste](https://docs.bunseed.org/docs/guide-utilisation-videaste/intro)
* Pour modifier le thème (développeur) : [Guide Technique](https://docs.bunseed.org/docs/platform-technique/intro)
